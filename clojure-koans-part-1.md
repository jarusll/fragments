---
title: Solving Clojurescript Koans - Part 1
date: 2022-04-05
tags: [youtube, updates]
---

# Solving Clojurescript Koans - Part 1

<iframe src="https://www.youtube.com/embed/9mst6f5__xg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

